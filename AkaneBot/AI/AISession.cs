﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkaneBot.AI
{
    public class AISession
    {

        public AISession(string name)
        {
            name_ = name;
        }

        private string name_ = "";
        public string NAME
        {
            get { return name_; }
            set { name_ = value; }
        }
    }
}

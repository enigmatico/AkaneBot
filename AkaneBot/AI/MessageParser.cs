﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Syn.Bot.Siml;
using Syn.EmotionML;
using AkaneBot.Bot;


namespace AkaneBot.AI
{
    public static class MessageParser
    {
        public static string LastUser = "";
        public static string[] ewds = new string[] { "goodbye", "good bye", "sayonara", "stop answering", "stop talking to me", "don't reply", "dont reply", "talk to someone else", "stop replying"};

        public static bool IsItToMe(string message, MessageEventArgs evt)
        {
            if (evt.Message.IsMentioningMe())
                return true;

            if (message.Contains(BotConfig.CLIENTNAME.ToLower()) || message.Contains(BotConfig.USERNAME.ToLower()))
                return true;

            if (message.Contains(BotPersona.NAME.ToLower()))
                return true;

            if (evt.User.Name == LastUser)
                return true;

            return false;
        }

        public static string Parse(MessageEventArgs evt)
        {
            Task tsk = new Task(async () =>
            {
                string msg = evt.Message.Text.ToLower();
                if (IsItToMe(msg, evt))
                {
                    if (BotConfig.TestingMode && evt.User.Name != BotConfig.BOTMASTER)
                    {
                        BotToDiscord.SendMessage("Sorry, I am bussy shorting some things out with the help of " + BotConfig.BOTMASTER + ". Try again some other time!", evt.Channel.Name, evt.Server.Name);
                        return;
                    }

                    AISession ses = AIManager.Search(evt.User.Name);
                    if (ses == null)
                        ses = AIManager.CreateSession(evt.User.Name);

                    if (msg.Contains("talk to me"))
                    {
                        LastUser = evt.User.Name;
                        BotUI.Log("Spotlight: " + LastUser);
                    }
                        

                    if (MessageParser.ewds.Any(msg.Contains) && evt.User.Name == LastUser)
                        MessageParser.LastUser = "";

                    BotUI.Log("Chat request: " + evt.Message.Text.Replace("@" + BotConfig.USERNAME, "") + "\nFrom: " + evt.User.Name);
                    ChatRequest cr = new ChatRequest(evt.Message.Text.Replace("@" + BotConfig.USERNAME, ""), AIManager.SearchBU(evt.User.Name));
                    ChatResult chatResult = AIManager.mBot.Chat(cr);
                    
                    await Task.Delay(new Random(evt.Message.Text[0] + evt.Message.Text[evt.Message.Text.Length - 1]).Next(1000, 10000));
                    string response = chatResult.BotMessage;
                    if (!string.IsNullOrEmpty(response))
                    {
                        try
                        {
                            await evt.Channel.SendIsTyping();
                            await Task.Delay(new Random(evt.Message.Text[0] + evt.Message.Text[evt.Message.Text.Length - 1]).Next(1000, 10000));
                            await evt.Channel.SendMessage(response);
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                        }
                    }
                    else
                        await evt.Channel.SendMessage("You left me speechless!");
                }
                else
                {

                }
            });

            tsk.Start();
            return "";
        }
    }
}

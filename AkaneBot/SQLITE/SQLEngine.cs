﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using AkaneBot.Bot;

namespace AkaneBot.SQLITE
{
    public static class SQLEngine
    {
        public static SQLiteConnection mSQL = null;
        private static SQLiteCommand mCMD = null;
        public static SQLiteDataReader mDR = null;
        public static bool Initialized = false;

        public static void Initialize()
        {
            try
            {
                BotUI.Log("Initializing SQLite engine...");
                mSQL = new SQLiteConnection("Data Source=" + BotConfig.SQLITEFOLDER + ";Version=3;");
                mSQL.Open();
                mCMD = new SQLiteCommand(@"SELECT * FROM sqlite_master;", mSQL);
                mDR = mCMD.ExecuteReader();
                if(!mDR.HasRows)
                {
                    BotUI.Log("The database seems empty. Must create a new one...");
                    FileStream fs = new FileStream(@"SQLITE\\INIT\\INITIALIZEDB.SQL", FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    while(!sr.EndOfStream)
                    {
                        string cmd = sr.ReadLine();
                        BotUI.Log("Query: " + cmd);
                        mCMD = new SQLiteCommand(cmd, mSQL);
                        mCMD.ExecuteNonQuery();
                    }
                    sr.Close();
                    Initialized = true;
                    BotUI.Log("Done!");
                }
                BotUI.Log("Done!");
                Initialized = true;
            }
            catch (Exception exc)
            {
                BotUI.LError("Error: " + exc.ToString());
                return;
            }
        }

        public static void SQLiteQuery(string query)
        {
            mCMD = new SQLiteCommand(query, mSQL);
            mDR = mCMD.ExecuteReader();
        }

        public static int UserExist(string usname)
        {
            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"SELECT * FROM USERS WHERE NAME = @param";
            mCMD.Parameters.Add( new SQLiteParameter("@param", usname));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return NewUser(usname);
            else
            {
                mDR.Read();
                return mDR.GetInt32(0);
            }
        }

        public static string GetUserById(int id)
        {
            SQLiteCommand mCMDx = new SQLiteCommand(mSQL);
            if (BotConfig.TestingMode)
                BotUI.Log("GetUserById " + id.ToString());
            mCMDx.CommandText = @"SELECT NAME FROM USERS WHERE IDNO = @param";
            mCMDx.Parameters.Add(new SQLiteParameter("@param", id));
            mCMDx.Connection = mSQL;
            SQLiteDataReader mDRR = null;
            mDRR = mCMDx.ExecuteReader();
            if (!mDRR.HasRows)
                return "Unknown";
            else
            {
                mDRR.Read();
                return mDRR.GetString(0);
            }
        }

        public static int NewUser(string usname)
        {
            mCMD = new SQLiteCommand();
            if (BotConfig.TestingMode)
                BotUI.Log("NewUser " + usname);
            mCMD.CommandText = @"INSERT INTO USERS(NAME) VALUES (@param)";
            mCMD.Parameters.Add(new SQLiteParameter("@param", usname));
            mCMD.Connection = mSQL;
            mCMD.ExecuteNonQuery();
            mCMD.CommandText = @"SELECT * FROM USERS WHERE NAME = @param";
            mCMD.Parameters.Add(new SQLiteParameter("@param", usname));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return -1;
            else
            {
                mDR.Read();
                BotUI.Log("Created new user " + usname + " ID " + mDR.GetInt32(0));
                return mDR.GetInt32(0);
            }
        }

        public static bool RemovePlaylistEntry(string playlist, string server, int song)
        {
            int listid = PlaylistExist(playlist, server);
            if (listid < 0)
                return false;
            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"DELETE FROM LISTENTRIES WHERE LISTPOS = @param1 AND LISTID = @param2";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", song));
            mCMD.Parameters.Add(new SQLiteParameter("@param2", listid));
            mCMD.Connection = mSQL;
            if (mCMD.ExecuteNonQuery() < 1)
                return false;

            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"UPDATE LISTENTRIES SET LISTPOS = LISTPOS - 1 WHERE LISTID = @param1 AND LISTPOS > @param2";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", listid));
            mCMD.Parameters.Add(new SQLiteParameter("@param2", song));
            mCMD.Connection = mSQL;

            return true;
        }

        public static int PlaylistExist(string pname, string server)
        {
            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"SELECT * FROM PLAYLISTS WHERE NAME = @param1 AND SERVER = @param2";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", pname));
            mCMD.Parameters.Add(new SQLiteParameter("@param2", server));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return -1;
            else
            {
                mDR.Read();
                return mDR.GetInt32(0);
            }
        }

        public static string[] GetPlaylistsFromServer(string server)
        {
            string[] result = new string[0];
            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"SELECT NAME FROM PLAYLISTS WHERE SERVER = @param1";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", server));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return null;
            else
            {
                while(mDR.Read())
                {
                    Array.Resize(ref result, result.Length + 1);
                    result[result.Length - 1] = mDR.GetString(0);
                }
                return result;
            }
        }

        public static bool IsPlaylistOwned(string uname, string pname, string server)
        {
            int pid = UserExist(uname);
            if (pid < 0)
                return false;

            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"SELECT OWNER FROM PLAYLISTS WHERE NAME = @param1 AND SERVER = @param2";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", pname));
            mCMD.Parameters.Add(new SQLiteParameter("@param2", server));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return false;
            else
            {
                mDR.Read();
                if (mDR.GetInt32(0) == pid)
                    return true;
                return false;
            }
        }

        public static PhenexListEntry[] GetPlaylist(string pname, string server)
        {
            PhenexListEntry[] result = new PhenexListEntry[0];
            int PID = PlaylistExist(pname, server);
            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"SELECT LISTPOS,NAME,URL,FPATH,OWNERID FROM LISTENTRIES WHERE LISTID = @param1 ORDER BY LISTPOS ASC";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", PID));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return null;
            else
            {
                while(mDR.Read())
                {
                    int rid = mDR.GetInt32(4);
                    string uname = GetUserById(rid);
                    PhenexListEntry nEnt = new PhenexListEntry(mDR.GetString(1), mDR.GetString(2), mDR.GetString(3), uname);
                    Array.Resize(ref result, result.Length + 1);
                    result[result.Length - 1] = nEnt;
                }
                return result;
            }
        }

        public static int NewPlaylist(string pname, string usname, string server)
        {
            int usid = UserExist(usname);
            if (usid < 0)
                usid = NewUser(usname);

            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"INSERT INTO PLAYLISTS (NAME,OWNER,SERVER) VALUES (@param1,@param2,@param3)";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", pname));
            mCMD.Parameters.Add(new SQLiteParameter("@param2", usid));
            mCMD.Parameters.Add(new SQLiteParameter("@param3", server));
            mCMD.Connection = mSQL;
            mCMD.ExecuteNonQuery();
            mCMD.CommandText = @"SELECT * FROM PLAYLISTS WHERE NAME = @param";
            mCMD.Parameters.Add(new SQLiteParameter("@param", pname));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return -1;
            else
            {
                mDR.Read();
                BotUI.Log("Created new playlist " + pname + " for user " + usname);
                return mDR.GetInt32(0);
            }
        }

        public static int NewListEntry(string title, string url, string fp, string usname, int playlistid)
        {
            int usid = UserExist(usname);
            if (usid < 0)
                usid = NewUser(usname);

            int lpos = 0;

            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"SELECT * FROM LISTENTRIES WHERE LISTID = @param";
            mCMD.Parameters.Add(new SQLiteParameter("@param", playlistid));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (mDR.HasRows)
                while (mDR.Read())
                    lpos++;

            mCMD = new SQLiteCommand();
            mCMD.CommandText = @"INSERT INTO LISTENTRIES(LISTPOS, NAME, URL, FPATH, OWNERID, LISTID) VALUES (@param1, @param2, @param3, @param6, @param4, @param5)";
            mCMD.Parameters.Add(new SQLiteParameter("@param1", lpos));
            mCMD.Parameters.Add(new SQLiteParameter("@param2", title));
            mCMD.Parameters.Add(new SQLiteParameter("@param3", url));
            mCMD.Parameters.Add(new SQLiteParameter("@param4", usid));
            mCMD.Parameters.Add(new SQLiteParameter("@param5", playlistid));
            mCMD.Parameters.Add(new SQLiteParameter("@param6", fp));
            mCMD.Connection = mSQL;
            mDR = mCMD.ExecuteReader();
            if (!mDR.HasRows)
                return -1;
            else
            {
                mDR.Read();
                BotUI.Log("Created new list entry " + title + " for playlist " + playlistid + " at position " + lpos);
                return mDR.GetInt32(0);
            }
        }
    }
}

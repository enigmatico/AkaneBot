﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace AkaneBot.Bot
{
    public static class BotCommands
    {
        public static Random rng = new Random(DateTime.Now.Second * DateTime.Now.Day + DateTime.Now.Millisecond);

        public static void HiCMD(string pam, CommandEventArgs evt)
        {
            if (string.IsNullOrEmpty(pam))
                BotToDiscord.SendMessage(string.Format(StringManager.GetString("hicmd"), evt.User.Name), evt.Channel.Name, evt.Server.Name);
            else
                BotToDiscord.SendMessage(string.Format(StringManager.GetString("hicmd"), pam), evt.Channel.Name, evt.Server.Name);
        }

        public static string AnswerYesNo(out int kind)
        {
            string result = "No answer given.";
            int rn = rng.Next(100);
            int answer = rng.Next(1,6);
            kind = 0;
            if(rn < 10)
            {
                kind = 0;
                result = StringManager.GetString("unknown" + answer.ToString());
            }
            else if(rn >= 10 && rn < 55)
            {
                kind = 1;
                result = StringManager.GetString("yes" + answer.ToString());
            }
            else if(rn >= 55)
            {
                kind = 2;
                result = StringManager.GetString("no" + answer.ToString());
            }
            return result;
        }

        public static int RollDice(int faces)
        {
            return rng.Next(1, faces);
        }

        public static void Emote(string pam, CommandEventArgs evt)
        {
            switch(pam)
            {
                case "ANGRY":
                    BotToDiscord.SendMessage(StringManager.GetString("angry"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/MAD.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "GLOOMY":
                    BotToDiscord.SendMessage(StringManager.GetString("gloomy"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/GLOOMY.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "GRIN":
                    BotToDiscord.SendMessage(StringManager.GetString("grin"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/GRIN.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "HAPPY":
                    BotToDiscord.SendMessage(StringManager.GetString("happy"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/HAPPY.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "NORMAL":
                    BotToDiscord.SendMessage(StringManager.GetString("normal"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/NORMAL.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "SAD":
                    BotToDiscord.SendMessage(StringManager.GetString("sad"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/SAD.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "SURPRISED":
                    BotToDiscord.SendMessage(StringManager.GetString("surprised"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/SURPRISED.png", evt.Channel.Name, evt.Server.Name);
                    break;
                case "SUSPICIOUS":
                    BotToDiscord.SendMessage(StringManager.GetString("suspicious"), evt.Channel.Name, evt.Server.Name);
                    BotToDiscord.SendFile(@"EMO/SUSPICIOUS.png", evt.Channel.Name, evt.Server.Name);
                    break;
                default:
                    BotToDiscord.SendMessage(StringManager.GetString("defemote"), evt.Channel.Name, evt.Server.Name);
                    break;
            }
        }
    }
}

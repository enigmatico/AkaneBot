﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Discord;
using Discord.Commands;
using Discord.Audio;
using AkaneBot.Bot;
using AkaneBot.Modules;
using AkaneBot.SQLITE;

namespace AkaneBot
{
    public static class BotToDiscord
    {
        public static DiscordClient botClient = new DiscordClient(x =>
        {
            x.AppName = BotConfig.CLIENTNAME;
            x.LogLevel = LogSeverity.Info;
        });

        public static void SendMessage(string message)
        {
            try
            {
                botClient.FindServers(BotConfig.DEFAULTSERVER).First().FindChannels(BotConfig.DEFAULTCHANNEL, ChannelType.Text, false).First().SendMessage(message);
            }catch(Exception exc)
            {
                //MessageBox.Show("Error: " + exc.ToString());
                BotUI.LError("Error: " + exc.ToString());
                return;
            }
        }

        public static void SendMessage(string message, string channel, string server)
        {
            try
            {
                botClient.FindServers(server).First().FindChannels(channel, ChannelType.Text, false).First().SendMessage(message);
            }
            catch (Exception exc)
            {
                //MessageBox.Show("Error: " + exc.ToString());
                BotUI.LError("Error: " + exc.ToString());
                return;
            }
        }

        public static void SendFile(string file, string channel, string server)
        {
            try
            {
                botClient.FindServers(server).First().FindChannels(channel, ChannelType.Text, false).First().SendFile(file);
            }
            catch (Exception exc)
            {
                //MessageBox.Show("Error: " + exc.ToString());
                BotUI.LError("Error: " + exc.ToString());
                return;
            }
        }

        public static void JoinVoiceChannel(string server, string channel)
        {
            Task t = new Task(async () => {
                try
                {
                    Channel c = botClient.FindServers(server).First().FindChannels(channel, ChannelType.Voice, false).First();
                    IAudioClient iau = await botClient.GetService<AudioService>().Join(c);
                    PhenexPlayer s = Phenex.SearchPlayerByServer(server);
                    if (s == null)
                    {
                        s = new PhenexPlayer(iau);
                        Phenex.InsertPlayer(s);
                    }
                    else
                    {
                        int pid = Phenex.GetPlayerIdByServer(server);
                        if(pid < 0)
                        {
                            BotUI.LError("Player not found: " + server);
                            return;
                        }
                        Phenex.NewAudioClient(iau, pid);
                    }
                        
                    BotUI.Log("Joined channel " + channel + " in server " + server + ". Player is ready.");
                }catch(Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }

            });
            t.Start();

        }

        static void c_SeverAvailable(object sender, ServerEventArgs e)
        {
            #if !DEBUG
            SendMessage(String.Format(StringManager.GetString("online"), BotPersona.NAME, BotPersona.SURNAME), BotConfig.DEFAULTCHANNEL, e.Server.Name);
            #endif
            ServerManager.NewServer(e.Server);
            BotUI.AddServerToUI(e.Server.Name);
            BotUI.Log("Server connected: " + e.Server.Name);
         }

        static void c_MessageReceived(object sender, MessageEventArgs e)
        {
            if (e.User.Name == BotConfig.CLIENTNAME || e.Message.Text.StartsWith("$"))
                return;

            AI.MessageParser.Parse(e);
        }

        public static void Init()
        {
            botClient.UsingCommands(x =>
            {
                x.PrefixChar = '$';
                x.HelpMode = HelpMode.Public;
                x.AllowMentionPrefix = true;
            });

            BotUI.Log("Command module initialized");

            botClient.UsingAudio(x => {
                x.Mode = AudioMode.Outgoing;
            });

            BotUI.Log("Audio module initialized");
            
            MakeCommands();
        }

        public static bool Connect()
        {
            bool error = false;
            botClient.ServerAvailable += c_SeverAvailable;
            botClient.MessageReceived += c_MessageReceived;
            botClient.ExecuteAndWait(async () => {
                try
                {
                    await botClient.Connect(BotConfig.OAUTH, TokenType.Bot);
                }
                catch (Exception exc)
                {
                    //MessageBox.Show("There was a problem connecting to the server.\n" + exc.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    BotUI.LError("There was a problem connecting to the server.\n" + exc.ToString());
                    error = true;
                }
            });
            BotUI.Log("Connected to discord. Bot is now online and ready!");
            return !error;
        }

        public static bool ServiceCheck(CommandEventArgs eas)
        {
            if (BotConfig.TestingMode && eas.User.Name != BotConfig.BOTMASTER)
            {
                SendMessage(string.Format(StringManager.GetBotString("service").TEXT, BotConfig.BOTMASTER), eas.Channel.Name, eas.Server.Name);
                return true;
            }
            return false;
        }

        public static void MakeCommands()
        {
            botClient.GetService<CommandService>().CreateCommand("hi")
                .Alias(new string[] { "ping" })
                .Description("Salute the user. It can act as a ping request.")
                .Parameter("User", ParameterType.Multiple)
                .Do(eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    string usr = null;
                    if(eas.Args.Length >= 1)
                        usr = eas.GetArg("User");
                    BotCommands.HiCMD(usr, eas);
                });

            botClient.GetService<CommandService>().CreateCommand("pose")
                .Alias(new string[] { "emote" })
                .Description("I will make a cool pose for you! Available poses are ANGRY, GLOOMY, GRIN, HAPPY, NORMAL, SAD, SURPRISED, and SUSPICIOUS!")
                .Parameter("Pose", ParameterType.Required)
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    BotUI.Log("CMD: pose");
                    string usr = null;
                    if (eas.Args.Length >= 1)
                        usr = eas.GetArg("Pose").ToUpper();
                    else
                    {
                        await Task.Delay(1000);
                        BotToDiscord.SendMessage(StringManager.GetString("badpose"), eas.Channel.Name, eas.Server.Name);
                        return;
                    }
                    await Task.Delay(1000);
                    BotCommands.Emote(usr, eas);
                });

            botClient.GetService<CommandService>().CreateCommand("dice")
            .Alias(new string[] { "roll", "dc" })
            .Description("Throw a dice. 6 faces by default.")
            .Parameter("Faces", ParameterType.Optional)
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;
                string pam = eas.GetArg("Faces");
                int faces = 6;
                if (!string.IsNullOrEmpty(pam))
                    faces = Convert.ToInt32(pam);
                if (faces < 1)
                    faces = 6;
                try
                {
                    int res = BotCommands.RollDice(faces);
                    await Task.Delay(1000);
                    await eas.Channel.SendMessage(eas.Message.User.Name + " rolled a dice! And got a " + res.ToString());
                    return;
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("coin")
            .Alias(new string[] { "ct" })
            .Description("Toss a coin. Gets HEADS or TAILS.")
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;
                try
                {
                    await Task.Delay(1000);
                    int res = BotCommands.rng.Next(2);
                    int acc = BotCommands.rng.Next(10);
                    int accr = BotCommands.rng.Next(1, 5);
                    if(acc >= 9)
                    {
                        await eas.Channel.SendMessage(eas.Message.User.Name + " " + StringManager.GetString("cmiss" + accr.ToString()));
                        return;
                    }
                    if (res == 1)
                        await eas.Channel.SendMessage(eas.Message.User.Name + " " + StringManager.GetString("cheads"));
                    else
                        await eas.Channel.SendMessage(eas.Message.User.Name + " " + StringManager.GetString("ctails"));
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("praise")
            .Alias(new string[] { "cheer" })
            .Description("I will cheer you up!")
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;
                try
                {
                    await Task.Delay(2500);
                    int accr = BotCommands.rng.Next(1, 5);
                    int exp = BotCommands.rng.Next(1,2);
                    switch(exp)
                    {
                        case 1:
                            await eas.Channel.SendFile(@"EMO/FACES/GRIN.png");
                            break;
                        case 2:
                            await eas.Channel.SendFile(@"EMO/FACES/HAPPY.png");
                            break;
                    }
                    await eas.Channel.SendMessage(string.Format(StringManager.GetString("praise" + accr.ToString()), eas.Message.User.Name));
                    return;
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("rps")
            .Description("Play Rock, Paper or scissors. Available hands are ROCK, PAPER, SCISSORS, or R, P, S")
            .Parameter("Hand", ParameterType.Required)
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;
                string hand = eas.GetArg("Hand").ToLower();
                BotUI.Log("RPS " + hand);
                try
                {
                    int res = BotCommands.rng.Next(1, 3); //1 R 2 P 3 S
                    int phand = 1;
                    string response = "";
                    switch (res)
                    {
                        case 1:
                            response += StringManager.GetString("irock");
                            break;
                        case 2:
                            response += StringManager.GetString("ipaper");
                            break;
                        case 3:
                            response += StringManager.GetString("iscissor");
                            break;
                    }

                    

                    if (hand == "rock" || hand == "r")
                        phand = 1;
                    else if (hand == "paper" || hand == "p")
                        phand = 2;
                    else if (hand == "scissors" || hand == "s")
                        phand = 3;
                    else
                    {
                        await Task.Delay(1000);
                        await eas.Channel.SendMessage(StringManager.GetString("badrps"));
                        return;
                    }

                    switch (phand)
                    {
                        case 1:
                            response += " " + StringManager.GetString("urock");
                            break;
                        case 2:
                            response += " " + StringManager.GetString("upaper");
                            break;
                        case 3:
                            response += " " + StringManager.GetString("uscissor");
                            break;
                    }

                    if((res == 1 && phand == 3) || (res == 2 && phand == 1) || (res == 3 && phand == 2))
                    {
                        await Task.Delay(1000);
                        await eas.Channel.SendFile("EMO/FACES/GRIN.png");
                        await eas.Channel.SendMessage(response + " " + StringManager.GetString("iwinrps"));
                        return;
                    }
                    else if(res == phand)
                    {
                        await Task.Delay(1000);
                        await eas.Channel.SendFile("EMO/FACES/SURPRISED.png");
                        await eas.Channel.SendMessage(response + " " + StringManager.GetString("tierps"));
                        return;
                    }
                    else
                    {
                        await Task.Delay(1000);
                        await eas.Channel.SendFile("EMO/FACES/GLOOMY.png");
                        await eas.Channel.SendMessage(response + " " + StringManager.GetString("uwinrps"));
                        return;
                    }
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("pic")
            .Description("Post a random, cute picture! (Safe for work)")
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;
                try
                {
                    await Task.Delay(2500);
                    DirectoryInfo DF = new DirectoryInfo(@"EMO/PICS");
                    FileInfo[] fi = DF.GetFiles();
                    int pic = BotCommands.rng.Next(0, fi.Length);
                    await eas.Channel.SendFile(fi[pic].FullName);
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("base64")
            .Alias(new string[] { "b64" })
            .Description("Encode a text into Base 64.")
            .Parameter("Text", ParameterType.Multiple)
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;

                try
                {
                    string fullmsg = String.Join(" ", eas.Args, 0, eas.Args.Length);
                    byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(fullmsg);
                    string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
                    await Task.Delay(1000);
                    await eas.Channel.SendMessage("Base 64: " + returnValue);
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("from64")
            .Alias(new string[] { "db64" })
            .Description("Decode a text from Base 64 to regular ASCII.")
            .Parameter("Text", ParameterType.Optional)
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;

                try
                {
                    string fullmsg = eas.GetArg("Text");
                    byte[] encodedDataAsBytes = System.Convert.FromBase64String(fullmsg);
                    string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
                    await Task.Delay(1000);
                    await eas.Channel.SendMessage("ASCII: " + returnValue);
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    Task.Delay(1000);
                    eas.Channel.SendMessage("That doesn't look like valid base 64.");
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateCommand("say")
            .Description("I will replicate a message in the specified room!")
            .Parameter("Channel", ParameterType.Required)
            .Parameter("Message", ParameterType.Multiple)
            .Do(eas =>
            {
                if (ServiceCheck(eas))
                    return;
                string fcmd = String.Join(" ", eas.Args, 1, eas.Args.Length - 1);
                BotToDiscord.SendMessage(eas.User.Name + ": " + fcmd, eas.GetArg(0), eas.Server.Name);
            });

            botClient.GetService<CommandService>().CreateCommand("yesno")
            .Alias(new string[] { "yn" })
            .Description("Ask me a yes or no question, and I will answer!")
            .Parameter("question", ParameterType.Multiple)
            .Do(async eas =>
            {
                if (ServiceCheck(eas))
                    return;
                try
                {
                    await Task.Delay(1000);
                    int k = 0;
                    string res = BotCommands.AnswerYesNo(out k);
                    if (k == 0)
                        await eas.Channel.SendFile(@"EMO/FACES/GLOOMY.png");
                    else if (k == 1)
                        await eas.Channel.SendFile(@"EMO/FACES/HAPPY.png");
                    else if (k == 2)
                        await eas.Channel.SendFile(@"EMO/FACES/SUSPICIOUS.png");
                    await eas.Channel.SendMessage(eas.Message.User.Name + ": " + res);
                }
                catch (Exception exc)
                {
                    BotUI.LError("Error: " + exc.ToString());
                    return;
                }
            });

            botClient.GetService<CommandService>().CreateGroup("playlist", cgb => {

                cgb.CreateCommand("create")
                .Alias(new string[] { "plc" })
                .Description("Create a brand new playlist for you in this server! Playlists are per server only.")
                .Parameter("Name", ParameterType.Required)
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    string cname = "undefined";
                    try
                    {
                        cname = eas.GetArg("Name");
                        int pid = SQLEngine.NewPlaylist(cname, eas.Message.User.Name, eas.Server.Name);
                        BotUI.Log("Created playlist " + cname + " ID " + pid.ToString());
                        await eas.Channel.SendMessage(string.Format(StringManager.GetString("plistready"), cname));
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        eas.Channel.SendMessage(string.Format(StringManager.GetString("pcreaterror"), cname));
                        return;
                    }
                });

                cgb.CreateCommand("load")
                .Alias(new string[] { "pld" })
                .Description("Load a playlist. Playlists are per server only!")
                .Parameter("name", ParameterType.Required)
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    string cname = "undefined";
                    try
                    {
                        cname = eas.GetArg("name");
                        PhenexPlayer pp = Phenex.SearchPlayerByServer(eas.Server.Name);
                        if(pp == null)
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("notinm"));
                            return;
                        }
                        pp.Stop();
                        int pid = Phenex.GetPlayerIdByServer(eas.Server.Name);
                        Phenex.GetPlayerList()[pid].LoadPlaylist(cname, eas.Server.Name);
                        await eas.Channel.SendMessage(string.Format(StringManager.GetString("plistload"), cname));
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        eas.Channel.SendMessage(string.Format(StringManager.GetString("plderror"), cname));
                        return;
                    }
                });

                cgb.CreateCommand("add")
                .Alias(new string[] { "pla" })
                .Description("Add a new item to the current playlist. You must own the playlist!")
                .Parameter("YoutubeURL", ParameterType.Required)
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    string cname = "undefined";
                    PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                    if (mp == null)
                    {
                        await eas.Channel.SendMessage(StringManager.GetString("notinm"));
                        return;
                    }
                    try
                    {
                        int usid = SQLEngine.UserExist(eas.Message.User.Name);
                        cname = eas.GetArg("YoutubeURL");
                        if(SQLEngine.IsPlaylistOwned(eas.Message.User.Name, mp.ListName, eas.Server.Name))
                        {
                            Phenex.AddToQueue(cname, eas.Message.User.Name, mp.ListName, eas.Server.Name, eas.Channel.Name);
                            await eas.Channel.SendMessage(string.Format(StringManager.GetString("queued"), Phenex.GetQueueSize(), Phenex.CurrentProgress.ToString()));
                            Phenex.ProcessQueue();
                        }
                        else
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("notowner"));
                            return;
                        }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                });

                cgb.CreateCommand("progress")
                .Alias(new string[] { "plcp" })
                .Description("Shows the progress of the queue, and a list of queued elements for this server.")
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    try
                    {
                        int p = 0;
                        await eas.Channel.SendMessage(string.Format(StringManager.GetString("questat"), Phenex.GetQueueSize(), Phenex.CurrentProgress.ToString()));
                        foreach(QueueElement qe in Phenex.QUEUE)
                        {
                            if(qe.SERVER == eas.Server.Name)
                                await eas.Channel.SendMessage((p + 1).ToString() + "/" + Phenex.GetQueueSize().ToString() + ": <" + qe.URL + ">");
                            p++;
                        }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                });

                cgb.CreateCommand("remove")
                .Alias(new string[] { "plrm" })
                .Description("Remove a specified entry from the current playlist. You must own the playlist!")
                .Parameter("Entry", ParameterType.Required)
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    int songid = 0;
                    PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                    if (mp == null)
                    {
                        await eas.Channel.SendMessage(StringManager.GetString("notinm"));
                        return;
                    }
                    try
                    {
                        string arg = eas.GetArg("Entry");
                        if (string.IsNullOrEmpty(arg))
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("invalidid"));
                            return;
                        }
                        else
                            songid = Convert.ToInt32(arg);

                        if(songid < 0)
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("invalidid"));
                            return;
                        }

                        int usid = SQLEngine.UserExist(eas.Message.User.Name);
                        if (SQLEngine.IsPlaylistOwned(eas.Message.User.Name, mp.ListName, eas.Server.Name))
                        {
                            if(SQLEngine.RemovePlaylistEntry(mp.ListName, eas.Server.Name, songid))
                                await eas.Channel.SendMessage(string.Format(StringManager.GetString("remele"), songid, mp.ListName));
                            else
                                await eas.Channel.SendMessage(StringManager.GetString("cantrem"));

                            if(mp.mPlay.PLAYLIST.Length > 0)
                                mp.mPlay.RemoveEntryById(songid);
                        }
                        else
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("notowner"));
                            return;
                        }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                });

                cgb.CreateCommand("showlists")
                .Alias(new string[] { "plls" })
                .Description("Show a list of available playlists for the current server.")
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    try
                    {
                        string[] lists = SQLEngine.GetPlaylistsFromServer(eas.Server.Name);

                        if(lists != null)
                            if(lists.Length > 0)
                            {
                                string full = String.Join("\n", lists);
                                await eas.Channel.SendMessage(string.Format(StringManager.GetString("plls"), full));
                                return;
                            }
                            else
                            {
                                await eas.Channel.SendMessage(StringManager.GetString("nopls"));
                                return;
                            }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                });

                cgb.CreateCommand("list")
                .Alias(new string[] { "pls" })
                .Description("List all the items in the current playlist.")
                .Do(async eas =>
                {
                    if (ServiceCheck(eas))
                        return;
                    try
                    {
                        PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                        if (mp == null)
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("notinm"));
                            return;
                        }
                        if(mp.mPlay.PLAYLIST.Length < 1)
                        {
                            await eas.Channel.SendMessage(StringManager.GetString("emptyls"));
                            return;
                        }
                        await eas.Channel.SendMessage(string.Format(StringManager.GetString("curls"), mp.ListName));
                        string list = "";
                        int items = 0;

                            foreach (PhenexListEntry le in mp.mPlay.PLAYLIST)
                            {
                                BotUI.Log("List: " + le.NAME);
                                list += items.ToString() + ": " + le.NAME + "\n";
                                items++;
                                if ((double)((double)items % 25.0) == 0.0 || items >= mp.mPlay.PLAYLIST.Length - 1)
                                {
                                    await eas.Channel.SendMessage("```" + list + " ```");
                                    await Task.Delay(10);
                                    list = "";
                                }

                            }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                });

            });

            botClient.GetService<CommandService>().CreateGroup("music", cgb => {
               
                    cgb.CreateCommand("join")
                    .Alias(new string[] { "mj" })
                    .Description("Join a voice channel in the current server")
                    .Parameter("channel", ParameterType.Required)
                    .Do(async eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        string cname = "undefined";
                        try
                        {
                            cname = eas.GetArg("channel");
                            BotToDiscord.JoinVoiceChannel(eas.Server.Name, cname);
                            await eas.Channel.SendMessage(string.Format(StringManager.GetString("vcj"), cname));
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            eas.Channel.SendMessage(string.Format(StringManager.GetString("vcjf"),cname));
                            return;
                        }
                    });

                    cgb.CreateCommand("leave")
                    .Alias(new string[] { "ml" })
                    .Description("(THIS COMMAND IS CURRENTLY DISABLED DUE TO A POSSIBLE BUG IN DISCORD.NET) Leave a voice channel in the current server")
                    .Do(async eas =>
                    {
                        return;
                        if (ServiceCheck(eas))
                            return;
                        try
                        {
                            int sid = Phenex.GetPlayerIdByServer(eas.Server.Name);
                            Phenex.GetPlayerList()[sid].Leave();
                            PhenexPlayer[] copy = new PhenexPlayer[Phenex.PLAYERLIST.Length - 1];
                            bool mark = false;
                            for (int x = 0; x < Phenex.PLAYERLIST.Length; x++)
                                if (x == sid)
                                {
                                    mark = true;
                                    continue;
                                }
                                else
                                    if (!mark)
                                        copy[x] = Phenex.PLAYERLIST[x];
                                    else
                                        copy[x - 1] = Phenex.PLAYERLIST[x];
                            Phenex.PLAYERLIST = copy;

                                await eas.Channel.SendMessage(StringManager.GetString("vcl"));
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            eas.Channel.SendMessage(StringManager.GetString("vclf"));
                            return;
                        }
                    });

                    cgb.CreateCommand("play")
                    .Alias(new string[] { "mp" })
                    .Description("Play a track in the playlist.")
                    .Parameter("Track number", ParameterType.Optional)
                    .Do(async eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        int songid = 0;
                        try
                        {
                            string arg = eas.GetArg("Track number");
                            if (string.IsNullOrEmpty(arg))
                                songid = 0;
                            else
                                songid = Convert.ToInt32(arg);

                            if (songid < 0)
                                songid = 0;
                            int sid = Phenex.GetPlayerIdByServer(eas.Server.Name);
                            if(Phenex.GetPlayerList()[sid].GetListSize() < 1)
                            {
                                await eas.Channel.SendMessage(StringManager.GetString("emptyls"));
                                return;
                            }

                            Phenex.GetPlayerList()[sid].Playback(songid);
                            PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                            int cid = mp.GetCurrentPlay();
                            PhenexListEntry ci = mp.mPlay.PLAYLIST[cid];

                            await eas.Channel.SendMessage(string.Format(StringManager.GetString("nowplaying"), cid.ToString(), ci.NAME, ci.REQUESTEDBY, ci.URL));
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            eas.Channel.SendMessage(StringManager.GetString("playerror"));
                            return;
                        }
                    });

                    cgb.CreateCommand("pause")
                    .Alias(new string[] { "mps" })
                    .Description("Pauses or unpauses the current playback. If any!")
                    .Do(async eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        try
                        {
                            int sid = Phenex.GetPlayerIdByServer(eas.Server.Name);
                            Phenex.GetPlayerList()[sid].Pause();
                            await eas.Channel.SendMessage(StringManager.GetString("togglep"));
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            return;
                        }
                    });


                    cgb.CreateCommand("stop")
                    .Alias(new string[] { "ms" })
                    .Description("Stops the current playback. If any!")
                    .Do(eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        try
                        {
                            int sid = Phenex.GetPlayerIdByServer(eas.Server.Name);
                            Phenex.GetPlayerList()[sid].Stop();

                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            return;
                        }
                    });

                    cgb.CreateCommand("next")
                    .Alias(new string[] { "mn" })
                    .Description("Jump to the next track in the list, or back to the first one if this is the last one.")
                    .Do(async eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        try
                        {
                            PhenexPlayer pp = Phenex.SearchPlayerByServer(eas.Server.Name);
                            BotUI.Log("Next for: " + eas.Server.Name + " " + pp.CHANNEL);
                            pp.Next();
                            PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                            int cid = mp.GetCurrentPlay();
                            PhenexListEntry ci = mp.mPlay.PLAYLIST[cid];

                            await eas.Channel.SendMessage(string.Format(StringManager.GetString("nowplaying"), cid.ToString(), ci.NAME, ci.REQUESTEDBY, ci.URL));
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            return;
                        }
                    });

                    cgb.CreateCommand("previous")
                    .Alias(new string[] { "mp" })
                    .Description("Jump to the previous track in the list, or back to the last one if this is the first one.")
                    .Do(async eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        try
                        {
                            PhenexPlayer pp = Phenex.SearchPlayerByServer(eas.Server.Name);
                            pp.Previous();
                            PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                            int cid = mp.GetCurrentPlay();
                            PhenexListEntry ci = mp.mPlay.PLAYLIST[cid];

                            await eas.Channel.SendMessage(string.Format(StringManager.GetString("nowplaying"), cid.ToString(), ci.NAME, ci.REQUESTEDBY, ci.URL));
                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            return;
                        }
                    });

                    cgb.CreateCommand("current")
                    .Alias(new string[] { "mc" })
                    .Description("Shows info about the current item being played")
                    .Do(async eas =>
                    {
                        if (ServiceCheck(eas))
                            return;
                        try
                        {
                            PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(eas.Server.Name)];
                            int cid = mp.GetCurrentPlay();
                            PhenexListEntry ci = mp.mPlay.PLAYLIST[cid];

                            await eas.Channel.SendMessage(string.Format(StringManager.GetString("nowplaying"), cid.ToString(), ci.NAME, ci.REQUESTEDBY, ci.URL));

                        }
                        catch (Exception exc)
                        {
                            BotUI.LError("Error: " + exc.ToString());
                            return;
                        }
                    });

            });

            BotUI.Log("Bot commands loaded and ready to use");
        }
    }
}

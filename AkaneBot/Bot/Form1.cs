﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AkaneBot.Bot;
using AkaneBot.AI;
using AkaneBot.SQLITE;
using AkaneBot.Modules;

namespace AkaneBot
{
    public partial class Form1 : Form
    {
        Task connectTask;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            BotUI.FORM = this;
            BotUI.Log("-- DATE " + DateTime.Today.ToLongDateString() + " --");
            BotUI.Log("-- STARTING ENGINE --");
            ConfigManager.InitConfigManager();
            StringManager.Init(@"CONFIG/" + BotConfig.LANGUAGEFILE);
            this.Text = BotConfig.CLIENTNAME;
            SQLEngine.Initialize();
            Phenex.Initialize();
            AIManager.Initialize();
            BotToDiscord.Init();
            if (BotConfig.TestingMode)
                BotUI.Log("- SERVICE MODE ON -");

            connectTask = new Task(() =>
            {
                if (!BotToDiscord.Connect())
                    this.Close();
            });
            connectTask.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            BotUI.Log("- BOT SHUTDOWN -");
            BotUI.Log("Disconnecting...");
            BotToDiscord.botClient.Disconnect();
            BotUI.Log("Unloading. Good night!");
            BotUI.End();
        }

        private void in__KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void in__KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                InternalCommands.Parse(in_.Text);
                in_.Text = "";
            }
        }

        private void in__TextChanged(object sender, EventArgs e)
        {

        }

        private void out__TextChanged(object sender, EventArgs e)
        {
            out_.SelectionStart = out_.Text.Length;
            out_.ScrollToCaret();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BotUI.FillChannelList(listBox1.SelectedItem.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Discord;

namespace AkaneBot.Bot
{
    public static class BotUI
    {
        private static Form1 mForm;
        private static FileStream flog = new FileStream(@"LOGS/LOG" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".LOG", FileMode.Append, FileAccess.Write);
        private static FileStream ferr = new FileStream(@"LOGS/ERROR" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".LOG", FileMode.Append, FileAccess.Write);
        private static StreamWriter outlog = new StreamWriter(flog);
        private static StreamWriter outerr = new StreamWriter(ferr);
        public static Form1 FORM
        {
            set { mForm = value; }
        }

        public static void Log(string msg)
        {
            System.Drawing.Color orcolor = mForm.out_.SelectionColor;
            mForm.out_.SelectionColor = System.Drawing.Color.LightGray;
            mForm.out_.AppendText(DateTime.Now.ToShortTimeString() + ": " + msg + "\n");
            outlog.WriteLineAsync(msg);
        }

        public static void LError(string msg)
        {
            System.Drawing.Color orcolor = mForm.out_.SelectionColor;
            mForm.out_.SelectionColor = System.Drawing.Color.Red;
            mForm.out_.AppendText(DateTime.Now.ToShortTimeString() + ": " + msg + "\n");
            outerr.WriteLineAsync(msg);
        }

        public static void AddServerToUI(string name)
        {
            mForm.listBox1.Items.Add(name);
            return;
        }

        public static void FillChannelList(string serv)
        {
            mForm.comboBox1.Items.Clear();
            ServerEntity se = ServerManager.SearchServer(serv);
            foreach (Channel ch in se.server.AllChannels)
                mForm.comboBox1.Items.Add(ch.Name);

            mForm.comboBox1.SelectedIndex = 0;
        }

        public static void End()
        {
            outlog.Flush();
            outerr.Flush();
            outlog.Close();
            outerr.Close();
        }
    }
}

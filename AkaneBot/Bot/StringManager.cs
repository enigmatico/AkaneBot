﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.IO;
using System.Windows.Forms;

namespace AkaneBot.Bot
{
    public class BotString
    {
        private string class_ = "";
        private string string_ = "";
        public string CLASS
        {
            get { return class_; }
            set { class_ = value; }
        }
        public string TEXT
        {
            get { return string_; }
            set { string_ = value; }
        }

        public BotString(string classname, string text)
        {
            this.class_ = classname;
            this.string_ = text;
        }
    }
    public static class StringManager
    {
        public static List<BotString> Strings = new List<BotString>();
        private static XmlDocument mDoc = new XmlDocument();
        private static string data;

        private static string ReadNode(string path)
        {
            XmlNode node = mDoc.DocumentElement.SelectSingleNode(path);
            return node.InnerText;
        }

        private static string ReadNode(string path, out string class_)
        {
            XmlNode node = mDoc.DocumentElement.SelectSingleNode(path);
            class_ = node.Name;
            return node.InnerText;
        }

        public static string GetString(string classname)
        {
            foreach(BotString e in Strings)
            {
                if (e.CLASS == classname)
                    return e.TEXT;
            }
            return "";
        }

        public static BotString GetBotString(string nodepath)
        {
            string class_ = "";
            string val_ = ReadNode(nodepath, out class_);
            return new BotString(class_, val_);
        }

        public static void ReadAllStrings(string file)
        {
            XDocument xdoc = XDocument.Load(@file);
            foreach(XNode xnod in xdoc.DescendantNodes())
            {
                if(xnod is XElement)
                {
                    XElement ele = (XElement)xnod;
                    if(!ele.HasElements)
                    {
                        Strings.Add(new BotString(ele.Name.LocalName, ele.Value));
                    }
                }
            }
            xdoc = null;
        }

        public static void Init(string file)
        {
            try
            {
                ReadAllStrings(@file);
                BotUI.Log("Loaded string database");
            }catch(Exception exc)
            {
                //MessageBox.Show("Error: " + exc.ToString());
                BotUI.LError("Error: " + exc.ToString());
                return;
            }
            return;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using AkaneBot.Bot;

namespace AkaneBot
{
    public static class ConfigManager
    {
        private static XmlDocument mDoc = new XmlDocument();
        private static string data;

        private static string ReadNode(string path)
        {
            XmlNode node = mDoc.DocumentElement.SelectSingleNode(path);
            return node.InnerText;
        }
        public static void InitConfigManager()
        {
            try
            {
                StreamReader sr = new StreamReader("CONFIG/config.xml");
                data = sr.ReadToEnd();
                sr.Close();
                mDoc.LoadXml(data);
                BotConfig.OAUTH = ReadNode("/bot/discord/oauth");
                BotConfig.CLIENTNAME = ReadNode("/bot/discord/clientname");
                BotConfig.CLIENTCODE = ReadNode("/bot/discord/client");
                BotConfig.SECRET = ReadNode("/bot/discord/secret");
                BotConfig.USERNAME = ReadNode("/bot/discord/username");
                BotConfig.BOTMASTER = ReadNode("/bot/discord/botmaster");
                BotConfig.DEFAULTSERVER = ReadNode("/bot/discord/defaultserver");
                BotConfig.DEFAULTCHANNEL = ReadNode("/bot/discord/defaultchannel");
                BotConfig.PREFIX = ReadNode("/bot/discord/cmdprefix");
                BotConfig.MUSICFOLDER = ReadNode("/bot/discord/musicfolder");
                BotConfig.SQLITEFOLDER = ReadNode("/bot/discord/sqlitefolder");
                BotConfig.SQLPASSWORD = ReadNode("/bot/discord/sqlpassword");
                BotConfig.LANGUAGEFILE = ReadNode("/bot/discord/language");
                BotConfig.MAXDOWNLOADBYTES = Convert.ToInt64(ReadNode("/bot/discord/maxdownload"));

                BotPersona.AGE = ReadNode("/bot/persona/age");
                BotPersona.COUNTRY = ReadNode("/bot/persona/country");
                BotPersona.GENDER = ReadNode("/bot/persona/gender");
                BotPersona.NAME = ReadNode("/bot/persona/name");
                BotPersona.SURNAME = ReadNode("/bot/persona/surname");

                BotPersona.BIRTHDAY = ReadNode("/bot/persona/birthday");
                BotPersona.BIRTHMONTH = ReadNode("/bot/persona/birthmonth");
                BotPersona.BIRTHYEAR = ReadNode("/bot/persona/birthyear");

                BotPersona.WEIGHT = ReadNode("/bot/persona/weight");
                BotPersona.BLOOD = ReadNode("/bot/persona/blood");

                BotPersona.BRAIN = ReadNode("/bot/persona/brain");
                BotPersona.AIMLFOLDER = ReadNode("/bot/persona/brainbits");

                data = null;
                BotUI.Log("Configuration file loaded");
            }
            catch (Exception exc)
            {
                //MessageBox.Show("Error " + exc.ToString());
                BotUI.LError("Error " + exc.ToString());
            }

        }
    }
    public static class BotPersona
    {
        private static string nam = "";
        private static string surnam = "";
        private static string age = "";
        private static string gender = "";
        private static string country = "";
        private static string birtday = "";
        private static string birthmonth = "";
        private static string birthyear = "";
        private static string weight = "";
        private static string height = "";
        private static string blood = "";
        private static string brain = "";
        private static string bbits = "";
        public static string NAME
        {
            get { return nam; }
            set { nam = value; }
        }
        public static string SURNAME
        {
            get { return surnam; }
            set { surnam = value; }
        }
        public static string AGE
        {
            get { return age; }
            set { age = value; }
        }
        public static string GENDER
        {
            get { return gender; }
            set { gender = value; }
        }
        public static string COUNTRY
        {
            get { return country; }
            set { country = value; }
        }
        public static string BIRTHDAY
        {
            get { return birtday; }
            set { birtday = value; }
        }
        public static string BIRTHMONTH
        {
            get { return birthmonth; }
            set { birthmonth = value; }
        }
        public static string BIRTHYEAR
        {
            get { return birthyear; }
            set { birthyear = value; }
        }
        public static string WEIGHT
        {
            get { return weight; }
            set { weight = value; }
        }
        public static string HEIGHT
        {
            get { return height; }
            set { height = value; }
        }
        public static string BLOOD
        {
            get { return blood; }
            set { blood = value; }
        }
        public static string BRAIN
        {
            get { return brain; }
            set { brain = value; }
        }
        public static string AIMLFOLDER
        {
            get { return bbits; }
            set { bbits = value; }
        }
    }

    public static class BotConfig
    {
        private static string clientc = "";
        private static string secr = "";
        private static string clientn = "";
        private static string oauthk = "";
        private static string usern = "";
        private static string defserv = "";
        private static string defchan = "";
        private static string cmdpref = "";
        private static string botmast = "";
        private static string musicf = "";
        private static string sqlf = "";
        private static string sqlpass = "";
        private static string langfile = "";
#if DEBUG
    public static bool TestingMode = true;
#else
    public static bool TestingMode = false;
#endif
        

        private static long maxdown_ = 0;

        public static string PREFIX
        {
            get { return cmdpref; }
            set { cmdpref = value; }
        }
        public static string CLIENTNAME
        {
            get { return clientn; }
            set { clientn = value; }
        }
        public static string OAUTH
        {
            get { return oauthk; }
            set { oauthk = value; }
        }
        public static string CLIENTCODE
        {
            get { return clientc; }
            set { clientc = value; }
        }
        public static string SECRET
        {
            get { return secr; }
            set { secr = value; }
        }
        public static string USERNAME
        {
            get { return usern; }
            set { usern = value; }
        }
        public static string DEFAULTSERVER
        {
            get { return defserv; }
            set { defserv = value; }
        }
        public static string DEFAULTCHANNEL
        {
            get { return defchan; }
            set { defchan = value; }
        }
        public static string BOTMASTER
        {
            get { return botmast; }
            set { botmast = value; }
        }
        public static string MUSICFOLDER
        {
            get { return musicf; }
            set { musicf = value; }
        }
        public static string SQLITEFOLDER
        {
            get { return sqlf; }
            set { sqlf = value; }
        }

        public static string SQLPASSWORD
        {
            get { return sqlpass; }
            set { sqlpass = value; }
        }

        public static long MAXDOWNLOADBYTES
        {
            get { return maxdown_; }
            set { maxdown_ = value; }
        }
        public static string LANGUAGEFILE
        {
            get { return langfile; }
            set { langfile = value; }
        }
    }
}

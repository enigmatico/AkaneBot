﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkaneBot.AI;
using AkaneBot.Bot;
using AkaneBot.Modules;
using AkaneBot.SQLITE;

namespace AkaneBot.Bot
{
    public static class InternalCommands
    {
        public static void Parse(string cmd)
        {
            string[] pams = cmd.Split(new char[] { ' ' });
            string[] subpams = cmd.Split(new char[] { '"' });
            switch(pams[0].ToLower())
            {
                case "service":
                    try
                    {
                        BotConfig.TestingMode = Convert.ToBoolean(pams[1]);
                        BotUI.Log("- SERVICE MODE: " + BotConfig.TestingMode.ToString() + " - ");
                        
                    }catch(Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "rlcbot":
                    try
                    {
                        BotUI.Log("Releasing all bot resources...");
                        AIManager.mBot.Release();
                        AIManager.mBot = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        BotUI.Log("Reloading AI...");
                        AIManager.Initialize();
                    }catch(Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "tryurl":
                    try
                    {
                        if(pams.Length < 5)
                        {
                            BotUI.Log("tryurl <url> <playlist> <channel> <\"server\">");
                            return;
                        }
                        Phenex.AddToQueue(pams[1], BotConfig.BOTMASTER, pams[2], subpams[1], pams[3]);
                        if(!Phenex.IsProcessing)
                            Phenex.ProcessQueue();
                    }catch(Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "say":
                    if (pams.Length < 4)
                    {
                        BotUI.Log("Command say: \"say <\"server...\"> <channel> <\"message...\">\"");
                        return;
                    }
                    subpams = cmd.Split(new char[] { '"' });
                    try
                    {
                        BotToDiscord.SendMessage(subpams[3], subpams[2].Replace(" ", ""), subpams[1]);
                        BotUI.Log("Sent message: " + subpams[3] + "\nServer: " + subpams[1] + "\nChannel: " + subpams[2].Replace(" ", ""));
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    
                    break;
                case "newuser":
                    try
                    {
                        if(!SQLEngine.Initialized)
                        {
                            BotUI.LError("SQL Engine not initialized.");
                            return;
                        }

                        if (SQLEngine.UserExist(pams[1]) >= 0)
                        {
                            BotUI.LError("User " + pams[1] + " already exists.");
                            return;
                        }

                        if (SQLEngine.NewUser(pams[1]) < 0)
                            BotUI.LError("Could not create new user: " + pams[1]);
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "makepl":
                    try
                    {
                        //makepl <name> <"server">
                        int pid = SQLEngine.NewPlaylist(pams[1], BotConfig.BOTMASTER, subpams[1]);
                        BotUI.Log("Created playlist " + pams[1] + " ID " + pid.ToString());
                    }catch(Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }

                    break;
                case "joinvc":
                    try
                    {
                        //joinvc <channel> <"server">
                        subpams = cmd.Split(new char[] { '"' });
                        BotToDiscord.JoinVoiceChannel(subpams[1], pams[1]);
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "loadpl":
                    try
                    {
                        //loadpl <name> <"server">
                        BotUI.Log("Loading " + pams[1] + " for " + subpams[1]);
                        PhenexPlayer pp = Phenex.SearchPlayerByServer(subpams[1]);
                        pp.LoadPlaylist(pams[1], subpams[1]);
                        int cnt = 0;
                        foreach(PhenexListEntry li in pp.mPlay.PLAYLIST)
                        {
                            BotUI.Log(cnt.ToString() + ": " + li.NAME + " (" + li.FILEPATH + ")");
                            cnt++;
                        }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "playm":
                    try
                    {
                        //playm <index(0)> <"server">
                        BotUI.Log("Playing " + pams[1] + " for " + subpams[1]);
                        PhenexPlayer pp = Phenex.SearchPlayerByServer(subpams[1]);
                        pp.Playback(Convert.ToInt32(pams[1]));
                        BotUI.Log("Now playing: " + pp.mPlay.GetEntryById(Convert.ToInt32(pams[1])).NAME + "\nFile: " + pp.mPlay.GetEntryById(Convert.ToInt32(pams[1])).FILEPATH);
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "nextm":
                    try
                    {
                        PhenexPlayer pp = Phenex.SearchPlayerByServer(subpams[1]);
                        pp.Next();
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "prevm":
                    try
                    {
                        PhenexPlayer pp = Phenex.SearchPlayerByServer(subpams[1]);
                        pp.Previous();
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "stopm":
                    try
                    {
                        //stopm <"server">
                        //playm <index(0)> <"server">
                        PhenexPlayer pp = Phenex.SearchPlayerByServer(subpams[1]);
                        pp.Stop();
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "players":
                    BotUI.Log("Player list:");
                    foreach(PhenexPlayer ppl in Phenex.GetPlayerList())
                    {
                        BotUI.Log(ppl.SERVER.Name);
                    }
                    break;
                case "sql":
                    try
                    {
                        if (!SQLEngine.Initialized)
                        {
                            BotUI.LError("SQL Engine not initialized.");
                            return;
                        }
                        BotUI.Log("Query " + subpams[1]);
                        SQLEngine.SQLiteQuery(subpams[1]);

                        if(!SQLEngine.mDR.HasRows)
                        {
                            BotUI.LError("Query No result.");
                            return;
                        }
                        else
                        {
                            var dr = SQLEngine.mDR;
                            string row = "";
                            string format = "|";
                            string[] vals = new string[dr.FieldCount];
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                vals[i] += MiscTools.centeredString(dr.GetName(i), 24);
                                format += "{" + i.ToString() + "}|";
                            }
                            BotUI.Log(String.Format(format, vals));
                            BotUI.Log(row);
                            while(dr.Read())
                            {
                                row = "";
                                for (int i = 0; i < dr.FieldCount; i++)
                                    vals[i]= MiscTools.centeredString(dr.GetValue(i).ToString(), 24);
                                    
                                BotUI.Log(String.Format(format, vals).PadLeft(12));
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        BotUI.LError("Error: " + exc.ToString());
                        return;
                    }
                    break;
                case "playerlist":
                    PhenexPlayer[] php = Phenex.GetPlayerList();
                    if(php == null)
                    {
                        BotUI.Log("THE PLAYER LIST IS NULL");
                        return;
                    }
                    if (php.Length < 1)
                    {
                        BotUI.Log("THE PLAYER LIST IS EMPTY");
                        return;
                    }
                    foreach(PhenexPlayer phpl in php)
                        if(phpl.SERVER != null)
                         BotUI.Log("Player for " + phpl.SERVER.Name + " in channel " + phpl.CHANNEL.Name);

                    break;
            }
        }
    }
}

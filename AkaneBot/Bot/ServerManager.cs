﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.API;
using Discord;
using AkaneBot.Bot;

namespace AkaneBot.Bot
{
    public class ServerEntity
    {
        public Server server = null;

        public ServerEntity(Server s)
        {
            server = s;
        }
    }
    public static class ServerManager
    {
        public static ServerEntity[] sList = new ServerEntity[0];

        public static ServerEntity NewServer(Server ms)
        {
            ServerEntity se = new ServerEntity(ms);
            Array.Resize(ref sList, sList.Length + 1);
            sList[sList.Length - 1] = se;
            return se;
        }

        public static ServerEntity SearchServer(string name)
        {
            foreach (ServerEntity se in sList)
                if (se.server.Name == name)
                    return se;
            return null;
        }
    }
}

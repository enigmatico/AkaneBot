﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using AkaneBot;
using AkaneBot.Bot;
using AkaneBot.SQLITE;
using YoutubeExtractor;
using Discord;
using Discord.Audio;
using NAudio;
using NAudio.Wave;
using NAudio.CoreAudioApi;

namespace AkaneBot.Modules
{
    public class QueueElement
    {
        public string URL = "";
        public string REQUESTER = "";
        public string LIST = "";
        public string SERVER = "";
        public string CHANNEL = "";

        public QueueElement(string url_, string req_, string lst_, string serv_, string ch_)
        {
            this.URL = url_;
            this.REQUESTER = req_;
            this.LIST = lst_;
            this.SERVER = serv_;
            this.CHANNEL = ch_;
        }
    }

    public enum PhenexError
    {
        SUCCESS = 0x0,
        INVALIDURL = 0x1,
        VIDEOISDOWN = 0x2,
        VIDEOTOOBIG = 0x3,
        DOWNLOADFAILED = 0x4,
        CONVERSIONFAILED = 0x5,
        CONNECTIONERROR = 0x6,
        NETWORKPROBLEMS = 0x7,
        STREAMERROR = 0x8,
        UNDEFINED = 0xFE,
        SQLITENOTINITIALIZED = 0xFF
    }

    public static class Phenex
    {
        public static bool IsEnabled = false, IsProcessing = false;
        private static QueueElement[] dqueue = new QueueElement[0];
        public static double CurrentProgress = 0.0;
        public static PhenexError LastError = 0;
        private static PhenexPlayer[] playerList = new PhenexPlayer[0];

        public static PhenexPlayer[] PLAYERLIST
        {
            get { return playerList; }
            set { playerList = value; }
        }

        public static QueueElement[] QUEUE
        {
            get { return dqueue; }
        }

        public static void Initialize()
        {
            if(!SQLEngine.Initialized)
            {
                BotUI.LError("Could not initialize Phenex: SQLite engine not initialized.");
                LastError = PhenexError.SQLITENOTINITIALIZED;
                return;
            }
            IsEnabled = true;
        }

        public static int GetQueueSize()
        {
            return dqueue.Length;
        }

        public static void AddToQueue(string URL, string REQ, string LST, string SRV, string CH)
        {
            Array.Resize(ref dqueue, dqueue.Length + 1);
            dqueue[dqueue.Length - 1] = new QueueElement(URL, REQ, LST, SRV, CH);
            BotUI.Log("Queued: " + URL);
        }

        public static void ProcessQueue()
        {
            if (IsProcessing)
            {
                BotUI.LError("Queue is already being processed");
                return;
            }
            BotUI.Log("Starting queue");
            if(dqueue.Length > 0)
            {
                Task t = new Task(() => {
                    Download(dqueue[0]);
                    dqueue = dqueue.Skip(1).ToArray();
                });
                t.Start();
                return;
            }
            else
            {
                BotUI.Log("Nothing else to process");
                IsProcessing = false;
                return;
            }
        }

        public static void Download(QueueElement mle)
        {
            if(!Regex.IsMatch(mle.URL, "(http|https):\\/\\/(www.youtube.com|youtu.be)\\/(watch\\?v=)*[A-Za-z0-9_.-]\\w+", RegexOptions.IgnoreCase))
            {
                BotUI.LError("Not a valid youtube URL: " + mle.URL);
                BotToDiscord.SendMessage(string.Format(StringManager.GetString("qnotproc"), mle.URL), mle.CHANNEL, mle.SERVER);
                //IsProcessing = false;
                LastError = PhenexError.INVALIDURL;
                return;
            }
            IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(mle.URL);
            if(!videoInfos.Any())
            {
                BotUI.LError("This video seems to be down or has been privatized: " + mle.URL);
                BotToDiscord.SendMessage(string.Format(StringManager.GetString("qnotproc2"), mle.URL), mle.CHANNEL, mle.SERVER);
                //IsProcessing = false;
                LastError = PhenexError.VIDEOISDOWN;
                return;
            }
                string videopath = "";
                Task t = new Task(async () => {
                    IsProcessing = true;
                    bool toobig = false;

                    VideoInfo video = videoInfos
                        .First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360);

                    string vtitle = video.Title;

                    foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                        vtitle = vtitle.Replace(c, '_');

                    if (!File.Exists(BotConfig.MUSICFOLDER + "\\" + vtitle + ".mp3"))
                    {
                        if (video.RequiresDecryption)
                            DownloadUrlResolver.DecryptDownloadUrl(video);

                        videopath = Path.Combine(BotConfig.MUSICFOLDER + "\\", vtitle + video.VideoExtension);
                        var videoDownloader = new VideoDownloader(video, Path.Combine(BotConfig.MUSICFOLDER + "\\" + vtitle + video.VideoExtension));

                        videoDownloader.DownloadStarted += (e, evt)=>{
                            if(videoDownloader.BytesToDownload >= BotConfig.MAXDOWNLOADBYTES)
                            {
                                BotUI.LError("This video is too big! Please try a smaller video. ");
                                BotToDiscord.SendMessage(string.Format(StringManager.GetString("qnotproc3"), mle.URL), mle.CHANNEL, mle.SERVER);
                                toobig = true;
                                LastError = PhenexError.VIDEOTOOBIG;
                                return;
                            }
                        };

                        CurrentProgress = 0.0;

                        videoDownloader.DownloadProgressChanged += (e, evt) => {
                            CurrentProgress = evt.ProgressPercentage;
                            if (CurrentProgress >= 99.0)
                                CurrentProgress = 99.0;
                        };

                        videoDownloader.Execute();
                        if(toobig)
                        {
                            File.Delete(Path.Combine(BotConfig.MUSICFOLDER + "\\" + vtitle + video.VideoExtension));
                            IsProcessing = false;
                            return;
                        }
                        while (videoDownloader.BytesToDownload != null) { await Task.Delay(1); }

                        if (!File.Exists(videopath))
                        {
                            BotUI.LError("Error: failed to download " + mle.URL + " into " + videopath);
                            BotToDiscord.SendMessage(string.Format(StringManager.GetString("qnotproc4"), mle.URL), mle.CHANNEL, mle.SERVER);
                            IsProcessing = false;
                            LastError = PhenexError.DOWNLOADFAILED;
                            return;
                        }

                        Process ExternalProcess = new Process();
                        ExternalProcess.StartInfo.FileName = "ffmpeg.exe";
                        ExternalProcess.StartInfo.Arguments = "-i \"" + videopath + "\" -y -c:a libmp3lame -ac 2 -q:a 2 \"" + BotConfig.MUSICFOLDER  + "\\" + vtitle + ".mp3\"";
                        ExternalProcess.StartInfo.UseShellExecute = false;
                        ExternalProcess.StartInfo.RedirectStandardOutput = true;
                        ExternalProcess.StartInfo.CreateNoWindow = true;
                        ExternalProcess.Start();
                        ExternalProcess.WaitForExit();

                        CurrentProgress = 100.0;

                        File.Delete(videopath);
                        if (!File.Exists(BotConfig.MUSICFOLDER  + "\\" + vtitle + ".mp3"))
                        {
                            BotUI.LError("Error: " + BotConfig.MUSICFOLDER + "\\" + vtitle + ".mp3 was not created.");
                            BotToDiscord.SendMessage(string.Format(StringManager.GetString("qnotproc5"), mle.URL), mle.CHANNEL, mle.SERVER);
                            IsProcessing = false;
                            LastError = PhenexError.CONVERSIONFAILED;
                            return;
                        }
                        BotUI.Log("Success!");

                        int RID = -1;
                        if ((RID = SQLEngine.UserExist(mle.REQUESTER)) < 0)
                            RID = SQLEngine.NewUser(mle.REQUESTER);

                        int PID = -1;
                        if ((PID = SQLEngine.PlaylistExist(mle.LIST, mle.SERVER)) < 0)
                            PID = SQLEngine.NewPlaylist(mle.LIST, mle.REQUESTER, mle.SERVER);

                        SQLEngine.NewListEntry(vtitle, mle.URL, BotConfig.MUSICFOLDER + "\\" + vtitle + ".mp3", mle.REQUESTER, PID);
                        PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(mle.SERVER)];
                        if (mp.ListName == mle.LIST)
                        {
                            PhenexListEntry ple = new PhenexListEntry(vtitle, mle.URL, BotConfig.MUSICFOLDER + "\\" + vtitle + ".mp3", mle.REQUESTER);
                            mp.mPlay.AddToList(ple);
                        }

                        BotToDiscord.SendMessage(string.Format(StringManager.GetString("qsproc"), vtitle), mle.CHANNEL, mle.SERVER);
                        IsProcessing = false;
                        LastError = PhenexError.SUCCESS;
                        ProcessQueue();
                        return;
                    }else{
                        BotUI.Log("File already exists. Recicling it.");

                        int RID = -1;
                        if ((RID = SQLEngine.UserExist(mle.REQUESTER)) < 0)
                            RID = SQLEngine.NewUser(mle.REQUESTER);

                        int PID = -1;
                        if ((PID = SQLEngine.PlaylistExist(mle.LIST, mle.SERVER)) < 0)
                            PID = SQLEngine.NewPlaylist(mle.LIST, mle.REQUESTER, mle.SERVER);

                        SQLEngine.NewListEntry(vtitle, mle.URL, BotConfig.MUSICFOLDER + "\\" + vtitle + ".mp3", mle.REQUESTER, PID);

                        PhenexPlayer mp = Phenex.GetPlayerList()[Phenex.GetPlayerIdByServer(mle.SERVER)];
                        if(mp.ListName == mle.LIST)
                        {
                            PhenexListEntry ple = new PhenexListEntry(vtitle, mle.URL, BotConfig.MUSICFOLDER + "\\" + vtitle + ".mp3", mle.REQUESTER);
                            mp.mPlay.AddToList(ple);
                        }

                        IsProcessing = false;
                        LastError = PhenexError.SUCCESS;
                        BotToDiscord.SendMessage(string.Format(StringManager.GetString("qsproc"), vtitle), mle.CHANNEL, mle.SERVER);
                        ProcessQueue();
                        return;
                    }
                });

                if (IsProcessing && dqueue.Length < 1)
                    IsProcessing = false;
                if(IsProcessing)
                    BotUI.Log("Bussy, waiting...");
                while (IsProcessing) { Task.Delay(1); }
                BotUI.Log("Starting...");
                t.Start();
                return;
            }

            public static void InsertPlayer(PhenexPlayer ppl)
            {
                Array.Resize(ref playerList, playerList.Length + 1);
                playerList[playerList.Length - 1] = ppl;
                return;
            }

            public static PhenexPlayer[] GetPlayerList()
            {
                return playerList;
            }

            public static void NewAudioClient(IAudioClient miau, int id)
            {
                playerList[id].NewAudioClient(miau);
                return;
            }

            public static int GetPlayerIdByServer(string servername)
            {
                int id = 0;
                foreach (PhenexPlayer pp in playerList)
                {
                    if (pp.SERVER.Name == servername)
                        return id;
                    id++;
                }
                return -1;
            }

            public static PhenexPlayer SearchPlayerByServer(string servername)
            {
                foreach(PhenexPlayer pp in playerList)
                {
                    if (pp.SERVER.Name == servername)
                        return pp;
                }
                return null;
            }
        }
    }
    
    public class PhenexListEntry
    {
        string name = "";
        string url = "";
        string path = "";
        string rby = "";

        public PhenexListEntry(string nam_, string url_, string path_m, string rby_)
        {
            name = nam_;
            url = url_;
            path = path_m;
            rby = rby_;
        }

        public string NAME
        {
            get { return name; }
        }
        public string URL
        {
            get { return url; }
        }
        public string FILEPATH
        {
            get { return path; }
        }
        public string REQUESTEDBY
        {
            get { return rby; }
        }
    }

    public class PhenexPlaylist
    {
        private PhenexListEntry[] mList = new PhenexListEntry[0];
        public PhenexListEntry[] PLAYLIST
        {
            get { return mList; }
        }

        public PhenexPlaylist()
        {
            mList = new PhenexListEntry[0];
        }

        public PhenexPlaylist(PhenexListEntry[] list)
        {
            mList = list;
        }

        public PhenexListEntry AddEntry(string name_, string url_, string fp_, string rqby_)
        {
            PhenexListEntry mLE = new PhenexListEntry(name_, url_, fp_, rqby_);
            Array.Resize(ref mList, mList.Length + 1);
            mList[mList.Length - 1] = mLE;
            return mLE;
        }

        public int GetListSize()
        {
            return mList.Length;
        }

        public void AddToList(PhenexListEntry me)
        {
            if (mList == null)
                mList = new PhenexListEntry[1];
            else
                Array.Resize(ref mList, mList.Length + 1);
            mList[mList.Length - 1] = me;
        }

        public void RemoveEntryById(int id)
        {
            if(id >= mList.Length || id < 0)
            {
                BotUI.LError("Index out of bounds: " + id.ToString());
                return;
            }
            PhenexListEntry[] mirror = new PhenexListEntry[0];
            for(int x = 0; x < mList.Length; x++)
            {
                Array.Resize(ref mirror, mirror.Length + 1);
                mirror[mirror.Length - 1] = mList[x];
            }
            mList = mirror;
            return;
        }

        public PhenexListEntry GetEntryById(int id)
        {
            if (id >= mList.Length || id < 0)
            {
                BotUI.LError("Index out of bounds: " + id.ToString());
                return null;
            }
            return mList[id];
        }
    }

    public class PhenexPlayer
    {
        PhenexSwitches mySwitches = new PhenexSwitches();
        public long LastError = 0;
        public string ListName = "Undefined";
        public PhenexPlaylist mPlay = new PhenexPlaylist();
        private IAudioClient mAClient = null;
        private Task CurrentPlayback = null, CurrentRoge = null;
        private int sel = 0;
        private bool CommandStop = false, NextPlease = false, brake = false;

        public void NewAudioClient(IAudioClient miau)
        {
            Stop();
            mAClient.OutputStream.Flush();
            mAClient.Disconnect();
            mAClient = miau;
            return;
        }

        public Server SERVER
        {
            get { return mAClient.Server; }
        }

        public Channel CHANNEL
        {
            get { return mAClient.Channel; }
        }

        public void LoadPlaylist(string name, string server)
        {
            if (BotConfig.TestingMode)
                BotUI.Log("Get Playlist " + name + " " + server);
            mPlay = new PhenexPlaylist(SQLEngine.GetPlaylist(name, server));
            ListName = name;
        }

        public int GetCurrentPlay()
        {
            return sel;
        }

        public int GetListSize()
        {
            return mPlay.GetListSize();
        }

        public PhenexPlayer(IAudioClient iau)
        {
            mAClient = iau;
        }

        public string GetServer()
        {
            return mAClient.Server.Name;
        }

        public void Leave()
        {
            Stop();
            this.mAClient.Disconnect();
            //mAClient.Disconnect();
            ListName = "Undefined";
            mPlay = new PhenexPlaylist();
            brake = true;
            //CurrentRoge.Dispose();
            CurrentRoge = null;
        }

        public void Play(string filePath)
        {
            //eas.Channel.SendMessage(string.Format(":musical_note: `**  ~ NOW PLAYING ~  ** \n{0}: {1}\nRequested by: {2}`\n :tv: Youtube: <{3}>", cid.ToString(), ci.NAME, ci.REQUESTEDBY, ci.URL));
            CurrentPlayback = new Task(async () =>
            {
                
                var channelCount = BotToDiscord.botClient.GetService<AudioService>().Config.Channels;
                var OutFormat = new WaveFormat(48000, 16, channelCount);
                using (var MP3Reader = new Mp3FileReader(filePath))
                    using (var resampler = new MediaFoundationResampler(MP3Reader, OutFormat))
                    {
                        resampler.ResamplerQuality = 15; // Set the quality of the resampler to 60, the highest quality
                        int blockSize = OutFormat.AverageBytesPerSecond / 50;
                        byte[] buffer = new byte[blockSize];
                        int byteCount;
                        BotUI.Log("PLAYING " + filePath + " IN " + mAClient.Server.Name + ": " + mAClient.Channel.Name);
                        while ((byteCount = resampler.Read(buffer, 0, blockSize)) > 0 && (mySwitches.Playback && !mySwitches.Stopped)) // Read audio into our buffer, and keep a loop open while data is present
                        {
                            if (byteCount < blockSize)
                            {
                                // Incomplete Frame
                                for (int i = byteCount; i < blockSize; i++)
                                    buffer[i] = 0;
                            }

                            mySwitches.IsPlaying = true;
                            while (mySwitches.Pause && mySwitches.Playback) { await Task.Delay(1); }

                            try
                            {
                                mAClient.Send(buffer, 0, blockSize); // Send the buffer to Discord
                            }catch(Exception exc)
                            {
                                BotUI.LError("Playback Error: " + exc.ToString());
                                mySwitches.IsPlaying = false;
                                Stop();
                                return;
                            }
                        }
                        BotUI.Log("DONE");
                    }
                mySwitches.IsPlaying = false;
                if (!CommandStop)
                {
                    NextPlease = true;
                    BotUI.Log("Requesting next song (" + NextPlease.ToString() + ")");
                }
                return;
            });

            if(CurrentRoge == null)
            {
                CurrentRoge = new Task(async () =>
                {
                    while (!brake)
                    {
                        if (NextPlease)
                        {
                            Next();
                            NextPlease = false;
                            BotUI.Log("Next ( " + NextPlease.ToString() + " )");
                        }
                        await Task.Delay(10);
                    }
                    brake = false;
                });
                CurrentRoge.Start();
            }

            CurrentPlayback.Start();
            
        }

        public void Stop()
        {
            if(mySwitches.Playback == false)
            {
                CurrentPlayback = null;
                return;
            }
            mySwitches.Stopped = true;
            mySwitches.Playback = false;
            mySwitches.Pause = false;
            CommandStop = true;
            if(CurrentPlayback != null)
                CurrentPlayback.Wait();
            CurrentPlayback = null;
            mAClient.Clear();
            mAClient.OutputStream.Dispose();
            CommandStop = false;
        }

        public void Next()
        {
            Stop();
            sel++;
            if (sel >= mPlay.GetListSize())
                sel = 0;
            Playback(sel);
        }

        public void Previous()
        {
            Stop();
            sel--;
            if (sel < 0)
                sel = mPlay.GetListSize() - 1;
            Playback(sel);
        }

        public void Pause()
        {
            mySwitches.Pause = !mySwitches.Pause;
        }

        public void Playback(int id)
        {
            sel = id;
            Stop();
            mySwitches.Playback = true;
            mySwitches.Pause = false;
            mySwitches.IsPlaying = false;
            mySwitches.Stopped = false;
            CommandStop = false;
            Play(mPlay.GetEntryById(id).FILEPATH);
        }
    }

    public class PhenexSwitches
    {
        public bool[] Switches = new bool[5];
        public bool Playback
        {
            get
            {
                return Switches[0];
            }
            set
            {
                Switches[0] = value;
            }
        }

        public bool IsPlaying
        {
            get
            {
                return Switches[1];
            }
            set
            {
                Switches[1] = value;
            }
        }
        public bool Pause
        {
            get
            {
                return Switches[2];
            }
            set
            {
                Switches[2] = value;
            }
        }
        public bool Stopped
        {
            get
            {
                return Switches[3];
            }
            set
            {
                Switches[3] = value;
            }
        }
        public bool Shuffle
        {
            get
            {
                return Switches[4];
            }
            set
            {
                Switches[4] = value;
            }
        }
    }
